package sml;

/**
 * Created by Rustam Drake on 2/28/16.
 */
public class BnzInstruction extends Instruction {
    private int op;
    private String label1;
    private String label2;

    public BnzInstruction(String label, String op) {
        super(label, op);
    }

    public BnzInstruction(String label1, String op, String label2, String dummy) {
        this(label1, "bnz");
        this.label1=label1;
        this.op = Integer.parseInt(op);
        this.label2=label2;
    }

    private int labelNumber(String s){
        String str="";
        for (int i=0;i<s.length();i++){
            if((s.charAt(i) >= '0') && (s.charAt(i) <= '9')){
                str+=s.charAt(i);
            }
        }
        return Integer.parseInt(str);
    }

    @Override
    public void execute(Machine m){
        int value = m.getRegisters().getRegister(op);
        if(value!=0){
            m.setPc(labelNumber(label1)-labelNumber(label2));
        }
    }

    @Override
    public String toString() {
        return super.toString() + " jump to "+ label2;
    }
}
