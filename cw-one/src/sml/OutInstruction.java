package sml;

/**
 * Created by Rustam Drake on 2/28/16.
 */
public class OutInstruction extends Instruction {
    //private int result;
    private int op;
    private int value;

    public OutInstruction(String label, String op) {
        super(label, op);
    }

    public OutInstruction(String label, String op, String dummy1, String dummy2) {
        this(label, "out");
        //this.result = result;
        this.op = Integer.parseInt(op);
    }

    @Override
    public void execute(Machine m) {
        this.value = m.getRegisters().getRegister(op);
        System.out.println("value at register "+op+" is "+value);

    }

    @Override
    public String toString() {
        return super.toString() + " register " + op ;
    }
}