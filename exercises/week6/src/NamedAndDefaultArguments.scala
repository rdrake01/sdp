import com.atomicscala.AtomicTest._
//q6,7
class SimpleTime(val hours: Int, val minutes: Int = 0)
//q8.9
class Planet(  val description: String, val moons: Int = 1, val name: String){
  def hasMoon = if( moons>0 ) true else false
}

//q10
class Item(val name:String, var price: Double){
  def cost(grocery: Boolean = false, medication: Boolean = false, rate: Double = 0.10): Double = {
    if (grocery || medication) price
    else price + (rate * price)
  }
}

object NamedAndDefaultArguments extends App{
  //q6
  val t = new SimpleTime(hours=5, minutes=30)
  t.hours is 5
  t.minutes is 30
  //q7
  val t2 = new SimpleTime(hours=10)
  t2.hours is 10
  t2.minutes is 0

  //q8
  val p = new Planet(name = "Mercury",
    description = "small and hot planet", moons = 0)
  p.hasMoon is false

  //q9
  val earth = new Planet(moons = 1, name = "Earth",
    description = "a hospitable planet")
  earth.hasMoon is true

  //q10
  val flour = new Item(name="flour", 4)
  flour.cost(grocery=true) is 4
  val sunscreen = new Item(
    name="sunscreen", 3)
  sunscreen.cost() is 3.3
  val tv = new Item(name="television", 500)
  tv.cost(rate = 0.06) is 530

}
