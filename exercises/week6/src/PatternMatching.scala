import com.atomicscala.AtomicTest._

object PatternMatching extends App{
  def forecast(value: Int):String={
    if (value==100) "Sunny"
    else if (value==80) "Mostly Sunny"
    else if (value==50) "Partly Sunny"
    else if (value==20) "Mostly Cloudy"
    else if (value==0) "Cloudy"
    else "Unknown"
  }

  forecast(100) is "Sunny"
  forecast(80) is "Mostly Sunny"
  forecast(50) is "Partly Sunny"
  forecast(20) is "Mostly Cloudy"
  forecast(0) is "Cloudy"
  forecast(15) is "Unknown"

  val sunnyData = Vector(100, 80, 50, 20, 0, 15)

  for (s<-sunnyData){
    println("forecast("+s+") is " + forecast(s))
  }
}
