import com.atomicscala.AtomicTest._


class Family(names: String*){

  def familySize(): Int = {
    var count = 0
    for (i <- names) {
      count = count + 1
    }
    count
  }
}

class FlexibleFamily(mother:String,father:String, children:String*){
  def familySize():Int={
    var count = 2
    for(i<-children){
      count=count+1
    }
    count
  }
}

object ClassArguments extends App{
  val family1 = new Family("Mum", "Dad", "Sally", "Dick")
  family1.familySize() is 4
  val family2 = new Family("Dad", "Mom", "Harry")
  family2.familySize() is 3

  val family3 = new FlexibleFamily("Mum", "Dad", "Sally", "Dick")
  family3.familySize() is 4
  val family4 = new FlexibleFamily("Dad", "Mom", "Harry")
  family4.familySize() is 3


  //q5
  def squareThem(values: Int*): Int ={
    var square=0
    for (i<-values){
      square = square + (i * i)
    }
    square
  }

  squareThem(2) is 4
  squareThem(2, 4) is 20
  squareThem(1, 2, 4) is 21
}
