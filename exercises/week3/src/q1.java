
interface A{default void method(){}}
interface B{}
class AB implements A,B{}

abstract class Z{protected void method() {}}
abstract class X implements A{}
class ZX extends Z{}

public class q1 {
	public static void main(String[] args) {
		A a=new AB();
		Z z=new ZX();
		a.method();
		z.method();
	}
	
	
}
