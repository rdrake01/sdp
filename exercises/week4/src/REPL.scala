

object REPL {
  def main(args: Array[String]) {
    val value = 17
    println(value)
    //value=20 cant overwrite a 'val' type
    val value2 ="ABC123"
    println(value2)
    //value2="DEF1234" cant overwrite a 'val' type
    val value3=15.56
    println(value3)
  }
}