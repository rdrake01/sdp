object Methods {
  def main(args: Array[String]) {
    //q1
    def getSquare(value: Int): Int = {
      val square = value * value
      println(square)
      return square
    }

    val a = getSquare(3)
    assert(a == 9)

    val b = getSquare(6)
    assert(b == 36)

    val c = getSquare(5)
    assert(c == 25)

    //q2
    def isArg1GreaterThanArg2(value1: Double, value2: Double): Boolean = {
      if (value1 > value2) {
        println(true)
        return true
      } else {
        println(false)
        return false
      }
    }

    val t1 = isArg1GreaterThanArg2(4.1, 4.12)
    assert(t1 == false)
    val t2 = isArg1GreaterThanArg2(2.1, 1.2)
    assert(t2 == true)

    //q3
    def manyTimesString(message: String, mult: Int): String = {
      var multMessage: String = ""
      var i = 0
      for (i <- 1 to mult) {
        multMessage += message
      }
      println(multMessage)
      return multMessage
    }

    val m1 = manyTimesString("abc", 3)
    assert("abcabcabc" == m1, "false")
    val m2 = manyTimesString("123", 2)
    assert("123123" == m2, "false")

   
  }
}

