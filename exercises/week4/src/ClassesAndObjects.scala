
object ClassesAndObjects extends App{
  //q1
  val r1= 1 to 10
  println(r1.step)

  val r2 = new Range(1,10,2)
  println(r2.step)

  //q2

  var s1 ="Sally"
  var s2 ="Sally"
  if (s1.equals(s2)) println("s1 and s2 are equal") else println("s1 and s2 are not equal")

}
