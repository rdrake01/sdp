

object Expressions {
  def main(args: Array[String]) {
    var sky1 = "sunny"
    var sky2 = "partly cloudy"
    var temp = 88
    var temp2 = 10
    //q1
    if (sky1 == "sunny" && temp > 80) {
      println("expression 1:" + true)
    } else println("expression 1:" + false)
    //q2
    if ((sky1 == "sunny" || sky2 == "partly clou") && temp > 80) {
      println("expression 2:" + true)
    } else println("expression 2:" + false)
    //q3
    if ((sky1 == "sunny" || sky2 == "partly clou") && (temp > 80 || temp2 < 20)) {
      println("expression 3:" + true)
    } else println("expression 3:" + false)
    //q4
    val fahrenheit = 100f
    println(fahrenheit)
    val fahrToCelsius = (fahrenheit - 32) * (5f / 9)
    println("question 4: " + fahrenheit + " Fahrenheit -> " + fahrToCelsius + " Celsius")
    //q5

    val celsius = fahrToCelsius
    val celToFahr = (celsius * 9 / 5) + 32
    println("question 5: " + celsius + " Celsius -> " + celToFahr + " Fahrenheiet")

  }
}