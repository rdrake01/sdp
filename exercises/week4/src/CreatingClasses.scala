class Hippo{}
class Lion{}
class Tiger{}
class Monkey{}
class Giraffe{}

object CreatingClasses extends App{
  //q1
  val hippo = new Hippo
  println(hippo)
  val lion = new Lion
  println(lion)
  val tiger = new Tiger
  println(tiger)
  val monkey = new Monkey
  println(monkey)
  val giraffe = new Giraffe
  println(giraffe)

  //q2
  val lion2 = new Lion
  println(lion2)
  val giraffe2= new Giraffe
  println(giraffe2)
  val giraffe3 = new Giraffe
  println(giraffe3)
  //they are located in a different location
}



